/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants;

import Utilities.Helper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.Bow.KittyKannon;
import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.EnchantBowType;
import me.ctharvey.ExtraEnchants.EnchantsBase.*;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author charles
 */
public class EnchantController implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityDeath(EntityDeathEvent event){
		if(event.getEntity() instanceof Ocelot){
			if(KittyKannon.spawnedKitties.contains((Ocelot)event.getEntity())){
				event.setDroppedExp(0);
				event.getDrops().clear();
				event.getEntity().getLocation().getWorld().createExplosion(event.getEntity().getLocation(), 2);
				KittyKannon.spawnedKitties.remove((Ocelot) event.getEntity());
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBowFireEvent(EntityShootBowEvent e){
		try {
			if(e.getEntity() instanceof Player){
				sendEvent(new EventPair(e.getClass().getName(), e), (Player) e.getEntity());
			}
		} catch (Exception ex) {
			Logger.getLogger(EnchantController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onProjectileHit(ProjectileHitEvent e){
		Projectile projectile = e.getEntity();
		if(projectile.getShooter() instanceof Player){
			try {
				sendEvent(new EventPair(e.getClass().getName(), e), (Player) projectile.getShooter());
			} catch (Exception ex) {
				Logger.getLogger(EnchantController.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}
	
	
    
    @EventHandler(priority = EventPriority.HIGHEST)
	public void onDamageDoneByPlayer(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Player){
            try {
                sendEvent(new EventPair(e.getClass().getName(), e), (Player) e.getDamager());
            } catch (Exception ex) {
                Logger.getLogger(EnchantController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
	}
    
    @EventHandler(priority = EventPriority.HIGHEST)
	public void onDamageDoneToPlayer(EntityDamageByEntityEvent e){
		if(e.getEntity() instanceof Player){
            try {
                sendEvent(new EventPair(e.getClass().getName(), e), (Player) e.getEntity());
            } catch (Exception ex) {
                Logger.getLogger(EnchantController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
	}
    
    private class EventPair{
        
        public String eventName;
        public Event event;

        public EventPair(String eventName, Event event) {
            this.eventName = eventName;
            this.event = event;
        }
        
    }
    
	private void sendEvent(EventPair e, Player player) throws Exception{
        List<ItemStack> items = new ArrayList<>();
        items.add(player.getItemInHand());
        items.addAll(Arrays.asList(player.getInventory().getArmorContents()));
        for(ItemStack item: items){
            List<BaseEnchantment> enchants = BaseEnchantment.getEEnchantments(player, item);
            if(enchants != null){
                for(BaseEnchantment enchantment: enchants){
                    if(enchantment != null){
                        EnchantSuperType type = enchantment.getSupertype();
                        if(type.getApplicableMaterials().contains(item.getType())){
                            if (type.getEventNamesUsed().contains(e.eventName)){
                                dispatchEvent(e.event, enchantment);
                            } 
                        } 
                        
                    }
                }
            }
        }
	}
    
    private void dispatchEvent(Event e, BaseEnchantment enchantment) {
        if(e instanceof EntityShootBowEvent) {
            enchantment.onBowFire(e);
        } else if(e instanceof ProjectileHitEvent){
            enchantment.onProjectileHit(e);
        } else if(e instanceof EntityDamageByEntityEvent){
            enchantment.onDamageDoneByPlayer(e);
            enchantment.onDamageDoneToPlayer(e);
        }
    }
    
    public static final List<Material> helmets = Arrays.asList(Material.GOLD_HELMET, 
            Material.IRON_HELMET, Material.DIAMOND_HELMET, Material.LEATHER_HELMET, 
            Material.SKULL, Material.SKULL_ITEM);
    
    public static final List<Material> chestplates = Arrays.asList(Material.GOLD_CHESTPLATE, 
            Material.IRON_CHESTPLATE, Material.DIAMOND_CHESTPLATE, Material.LEATHER_HELMET);
    
    public static final List<Material> leggings = Arrays.asList(Material.GOLD_LEGGINGS,
            Material.IRON_LEGGINGS, Material.DIAMOND_LEGGINGS, Material.LEATHER_LEGGINGS);
    
    public static final List<Material> boots = Arrays.asList(Material.GOLD_LEGGINGS, 
            Material.IRON_BOOTS, Material.DIAMOND_BOOTS, Material.LEATHER_BOOTS);
}

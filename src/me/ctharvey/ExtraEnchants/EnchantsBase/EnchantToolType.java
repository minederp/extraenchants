/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase;

import java.util.List;
import org.bukkit.Material;

/**
 *
 * @author charles
 */
public enum EnchantToolType implements EnchantSuperType  {
	
	PICKAXE,
	AXE,
	HOE,
	SHOVEL,
	FLINT_AND_STEEL,
	BUCKET,
	COMPASS,
	CLOCK,
	FISHING_ROD,
	CARROT_ON_A_STICK,
	SHEARS,
	FIRE_CHARGE;

	public String name;
	
	public int id;
	
	public EnchantItemType enchantItemType = EnchantItemType.TOOL;
	
	EnchantToolType(){
		this.name = this.toString().toLowerCase();
		this.id = ordinal();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public EnchantItemType getEnchantItemType() {
		return this.enchantItemType;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public Index[] getSubtype() {
		return null;
	}

    @Override
    public List<Material> getApplicableMaterials() {
        return null;
    }

    @Override
    public List<String> getEventNamesUsed() {
        return null;
    }
	
}

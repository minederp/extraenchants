/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase;

import Utilities.Helper;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author charles
 */
public abstract class BaseEnchantment implements Listener {
	
    protected EnchantInfo info;
	protected String enchantmentMark;
	protected EnchantItemType enchantItemType;
	protected EnchantSuperType supertype;
	protected List<Material> applicableMaterials;
	protected Player player;
	protected ItemStack is;

    public BaseEnchantment(EnchantInfo info, EnchantItemType enchantItemType, EnchantSuperType enchantSuperType, Player player, ItemStack is){
        this.enchantItemType = enchantItemType;
        this.supertype = enchantSuperType;
        this.applicableMaterials = this.supertype.getApplicableMaterials();
        this.player = player;
        this.is = is;
        this.info = info;
        if(info.getCurrentLVL() > info.getMaxLVL() && !player.isOp()){
            info.setCurrentLVL(info.getMaxLVL());
        }
    }
	
	public EnchantItemType getEnchantItemType() {
		return enchantItemType;
	}

	public String getName() {
		return info.getName();
	}

	public EnchantSuperType getSupertype() {
		return supertype;
	}

	public String getEnchantmentMark() {
		return enchantmentMark;
	}

    public String getEnchantmentDisplayname(){
        return info.getColor()+info.getName()+" "+info.getCurrentLVL();
    }
	
	public  void activate(){
		ItemMeta im = is.getItemMeta();
		List<String> lore = im.getLore();
		if(lore == null){
			lore = new ArrayList<>();
		}
		if(!lore.contains(getEnchantmentDisplayname())) {
			lore.add(getEnchantmentDisplayname());
			im.setLore(lore);
			is.setItemMeta(im);
			Helper.send(player, "You just enchanted a "+is.getType()+" with "+getEnchantmentDisplayname());
		} else {
			lore.remove(getEnchantmentDisplayname());
			List<String> removeMe = new ArrayList<>();
			for(String loreItem: lore){
				if(loreItem.contains(info.getName())){
					removeMe.add(loreItem);
				}
			}
			for(String loreItem: removeMe){
				lore.remove(loreItem);
			}
			lore.add(getEnchantmentDisplayname());
			im.setLore(lore);
			is.setItemMeta(im);
			Helper.send(player, "You just enchanted a "+is.getType()+" with "+getEnchantmentDisplayname());
		}
	};
	
	public void deactivate(){
		List<String> lore = is.getItemMeta().getLore();
		if(lore.contains(getEnchantmentDisplayname())){
			lore.remove(getEnchantmentDisplayname());
		}
	};
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onItemStackInteract(PlayerInteractEvent e){
        
    }
	
	
    
	public final void onBowFire(Event e){
        EntityShootBowEvent event = (EntityShootBowEvent) e;
        onBowFire((EntityShootBowEvent) e);
    };
    
    public final void onProjectileHit(Event e){
        onProjectileHit((ProjectileHitEvent) e);
    }
    
	public void onBowFire(EntityShootBowEvent e){};
	
	protected void gettingDamaged(EntityDamageByEntityEvent e){};
	
	protected void givingDamage(EntityDamageByEntityEvent e){};

	public String getDescription() {
        return info.getDescription();
	}

	public void onProjectileHit(ProjectileHitEvent event){};

    public void onDamageDoneByPlayer(Event e) {
        onDamageDoneByPlayer((EntityDamageByEntityEvent) e);
    }
    
    public void onDamageDoneByPlayer(EntityDamageByEntityEvent e) {
    }
    
    public static List<BaseEnchantment> getEEnchantments(Player player, ItemStack itemInHand) {
        if(!itemInHand.hasItemMeta()) return null;
        if(!itemInHand.getItemMeta().hasLore()) return null;
        List<BaseEnchantment> enchants = new ArrayList();
        String eEnchantName;
        int level;
        for(String lore: itemInHand.getItemMeta().getLore()){
            eEnchantName = parseEnchantName(lore);
            level = parseCurrentLevel(lore);
            if(!eEnchantName.isEmpty() || level != 0) {
                enchants.add(createEnchantment(eEnchantName, player, level, itemInHand));
            }
        }
        return enchants;
    }
    
    private static String parseEnchantName(String lore){
		String split = ChatColor.stripColor(lore.split(" ")[0]);
		return split;
	}
	
	private static int parseCurrentLevel(String lore){
		String[] split = lore.split(" ");
		if(split.length < 2){
			return 0;
		} try{
            return Integer.parseInt(split[1]);
        } catch (Exception e){
            return 0;
        }
	}
    
    public static BaseEnchantment createEnchantment(String enchantName, Player player, int currentLVL, ItemStack is) {
		for(EnchantItemType type: EnchantItemType.values()){
			BaseEnchantment enchantment = type.getEnchantment(enchantName, player, currentLVL, is);
			if(enchantment != null){
				return enchantment;
			}
		}
		return null;
	}

    public final void onDamageDoneToPlayer(Event e){
        onDamageDoneToPlayer((EntityDamageByEntityEvent) e);
    }
    
    public void onDamageDoneToPlayer(EntityDamageByEntityEvent e) {
        
    }

}

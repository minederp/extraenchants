/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase;

import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.BowEnchantIndex;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author charles
 */
public interface Index {
	
	
	public BaseEnchantment getEnchantment(Player player, int currentLVL, ItemStack is);
	
	public String getName();
	
	public Index[] getSubtypes();
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase;

import java.util.List;
import org.bukkit.Material;

/**
 *
 * @author charles
 */
public interface EnchantSuperType {
	
	public String getName();
	
	public EnchantItemType getEnchantItemType();
	
	public int getId();
	
	public Index[] getSubtype();
	public List<Material> getApplicableMaterials();
    public List<String> getEventNamesUsed();
}

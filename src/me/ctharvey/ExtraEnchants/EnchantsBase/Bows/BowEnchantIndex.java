/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase.Bows;

import Utilities.Helper;
import me.ctharvey.ExtraEnchants.EnchantsBase.BaseEnchantment;
import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.Bow.BoomBow;
import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.Bow.CactusBow;
import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.Bow.EnderBow;
import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.Bow.FireworkBow;
import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.Bow.KittyKannon;
import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.Bow.Shotgun;
import me.ctharvey.ExtraEnchants.EnchantsBase.Index;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author charles
 */
public enum BowEnchantIndex implements Index{
	
	SHOTGUN,
	ENDERBOW,
	FIREWORKBOW,
	BOOMBOW,
	CACTUSBOW,
	KITTYKANNON;
	
	
	BowEnchantIndex(){
		this.name = this.toString();
		this.id = ordinal();
	}
	
	private String name;
	private int id;
	private EnchantBowType type = EnchantBowType.BOW;
	
	public static BowEnchantIndex getIndex(String name){
		for(BowEnchantIndex type: BowEnchantIndex.values()){
			if(name.equalsIgnoreCase(type.name)){
				return type;
			}
		}
		return null;
	}
	
	@Override
	public BaseEnchantment getEnchantment(Player player, int currentLVL, ItemStack is){
		switch(this){
			case SHOTGUN:
				return new Shotgun(player, is, currentLVL);
			case ENDERBOW:
				return new EnderBow(player, is, currentLVL);
			case FIREWORKBOW:
				return new FireworkBow(player, is, currentLVL);
			case BOOMBOW:
				return new BoomBow(player, is, currentLVL);
			case CACTUSBOW:
				return new CactusBow(player, is, currentLVL);
			case KITTYKANNON:
				return new KittyKannon(player, is, currentLVL);
			default:
				return null;
		}
		
	}

	@Override
	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	@Override
	public Index[] getSubtypes() {
		return values();
	}

}

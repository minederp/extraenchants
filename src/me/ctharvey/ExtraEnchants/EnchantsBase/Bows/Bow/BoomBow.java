/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase.Bows.Bow;

import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.EnchantBowType;
import java.util.Random;
import me.ctharvey.ExtraEnchants.EnchantsBase.BaseEnchantment;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantInfo;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantItemType;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author charles
 */
public class BoomBow extends BaseBowEnchantment {

	public BoomBow(Player player, ItemStack is, int currentLVL) {
        super(new EnchantInfo("BoomBow", "Arrow go boom on hit.", ChatColor.RED, currentLVL, 3), player, is);
	}

	@Override
	public void onItemStackInteract(PlayerInteractEvent e) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void onBowFire(EntityShootBowEvent e) {
		if(e.getEntity().equals(player)){
			fireBoomStick(e);
		}
	}

	@Override
	protected void gettingDamaged(EntityDamageByEntityEvent e) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	protected void givingDamage(EntityDamageByEntityEvent e) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	private void fireBoomStick(EntityShootBowEvent e) {
		Random random = new Random();
		Projectile proj = (Projectile) e.getProjectile();
		for(int x = 0; x < getAmount(); x++){
			TNTPrimed tnt = proj.getLocation().getWorld().spawn(proj.getLocation().add(random.nextInt(3)-1, 0, random.nextInt(3)-1), TNTPrimed.class);
			tnt.setVelocity(proj.getVelocity().multiply(random.nextDouble()));
			tnt.setFuseTicks(50);
		}
		e.setCancelled(true);
	}

	@Override
	public void onProjectileHit(ProjectileHitEvent event) {
	}
	
	private int getAmount(){
		switch(info.getCurrentLVL()){
			case 0:
				return 1;
			case 1: return 1;
			case 2: return 2;
			case 3: return 3;
			default: return 3 * info.getCurrentLVL();
		}
	}

}

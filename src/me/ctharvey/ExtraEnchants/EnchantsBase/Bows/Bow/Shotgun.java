/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase.Bows.Bow;

import java.util.Random;
import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.EnchantBowType;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantInfo;
import org.bukkit.ChatColor;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

/**
 *
 * @author charles
 */
public class Shotgun extends BaseBowEnchantment {

	private static final EnchantBowType type = EnchantBowType.BOW; 

	
	public Shotgun(Player player, ItemStack is, int currentLevel){
        super(new EnchantInfo("Shotgun", "Allows you to shoot multiple arrows at once.", ChatColor.DARK_BLUE, currentLevel, currentLevel), player, is);
	}
	
	@Override
	public void onBowFire(EntityShootBowEvent e){
		if(e.getEntity().equals(player)){
			fireShotgun(e);
		}
	}

	private void fireShotgun(EntityShootBowEvent e){
		Random random = new Random();
		for(int x = 0; x < getAmount(); x++){
			Arrow arrow = player.launchProjectile(Arrow.class);
			arrow.setVelocity(e.getProjectile().getVelocity());
			arrow.setVelocity(new Vector(
				arrow.getVelocity().getX()+((random.nextFloat()*.5-random.nextFloat()*.5)),
				arrow.getVelocity().getY()+((random.nextFloat()*.5-random.nextFloat()*.5)),
				arrow.getVelocity().getZ()+((random.nextFloat()*.5-random.nextFloat()*.5))
			));
		}
	}
	
	private int getAmount(){
		switch(info.getCurrentLVL()){
			case 0:
				return 0;
			case 1:
				return 2;
			case 2:
				return 4;
			case 3:
				return 7;
			case 4:
				return 11;
			default:
				return 3 * info.getCurrentLVL();
		}
	}

	@Override
	public void onProjectileHit(ProjectileHitEvent event) {
	}
	
}

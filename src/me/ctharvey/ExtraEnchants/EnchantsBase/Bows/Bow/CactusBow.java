/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase.Bows.Bow;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantInfo;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author charles
 */
public class CactusBow extends BaseBowEnchantment {
	
	public static List<Projectile> arrowsFired = new ArrayList<>();

	public CactusBow(Player player, ItemStack is, int currentLVL) {
        super(new EnchantInfo("CactusBow", "Plants cactus where it lands...", ChatColor.GREEN, currentLVL, 4), player, is);
	}

	@Override
	public void onBowFire(EntityShootBowEvent e) {
		if(e.getEntity() instanceof Player){
			arrowsFired.add((Projectile) e.getProjectile());
		}
	}

	@Override
	public void onProjectileHit(ProjectileHitEvent event) {
		Random random = new Random();
		if(arrowsFired.contains(event.getEntity())){
			for(int x = 0; x < getAmount(); x++){
				Block block = event.getEntity().getLocation().add(random.nextInt(5)-2.5, random.nextInt(3), random.nextInt(5)-2.5).getBlock();
				if(block.getType().equals(Material.AIR)){
//					block.setType(Material.CACTUS);
				}
			}
			arrowsFired.remove(event.getEntity());
		}
	}
	
	private int getAmount(){
		int v =0;
		switch(info.getCurrentLVL()){
			case 0: return v;
			case 1: return v=1;
			case 2: return v=2;
			case 3: return v=4;
			default: return v=6;
		}
	}
	
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase.Bows.Bow;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantInfo;
import me.ctharvey.ExtraEnchants.ExtraEnchants;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author charles
 */
public class KittyKannon extends BaseBowEnchantment {
	
	public static List<Ocelot> spawnedKitties = new ArrayList<>();

	public KittyKannon(Player player, ItemStack is, int currentLVL) {
        super(new EnchantInfo("KittyKannon", "MEOW PEW PEW", ChatColor.BLACK, currentLVL, 3), player, is);
	}

	@Override
	public void onBowFire(EntityShootBowEvent e) {
		if(e.getEntity() instanceof Player){
			Random random = new Random();
			for(int x = 0; x < getAmount(); x++){
				Ocelot ocelot = (Ocelot) player.getLocation().getWorld().spawnEntity(e.getProjectile().getLocation(), EntityType.OCELOT);
				ocelot.setVelocity(e.getProjectile().getVelocity().multiply(2));
				e.setCancelled(true);
				spawnedKitties.add(ocelot);
				Bukkit.getScheduler().scheduleSyncDelayedTask(ExtraEnchants.getPlugin(), new KittyBoomBoom(ocelot), 100L);
			}
		}
	}

	public int getAmount(){
		switch(info.getCurrentLVL()){
			case 0: return 0;
			case 1: return 1;
			case 2: return 2;
			case 3: return 4;
			default: return 6;
		}
	}

    @Override
    public void onProjectileHit(ProjectileHitEvent e) {};
	
	public class KittyBoomBoom implements Runnable{
		
		public KittyBoomBoom(Ocelot ocelot){
			this.ocelot = ocelot;
		}
		
		private Ocelot ocelot;

		public void setOcelot(Ocelot ocelot) {
			this.ocelot = ocelot;
		}

		@Override
		public void run() {
			ocelot.damage(ocelot.getMaxHealth());
		}
		
		
		
	}
	
}

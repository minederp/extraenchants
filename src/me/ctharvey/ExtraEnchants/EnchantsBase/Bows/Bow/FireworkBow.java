/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase.Bows.Bow;

import java.util.Random;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantInfo;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.util.Vector;

/**
 *
 * @author charles
 */
public class FireworkBow extends BaseBowEnchantment {

	public FireworkBow(Player player, ItemStack is, int currentLVL) {
        super(new EnchantInfo("FireworkBow", "Put on a good show for players!", ChatColor.DARK_RED, currentLVL, 4), player, is);
	}

	@Override
	public void onBowFire(EntityShootBowEvent e) {
		if(e.getEntity().equals(player)){
			fireFireWorks(e);
		}
	}
	
	private void fireFireWorks(EntityShootBowEvent e){
		e.setCancelled(true);
		Entity projectile = e.getProjectile();
		Random random = new Random();
		for(int x = 0; x < getNumFireworks(); x++){
			Firework firework = player.getWorld().spawn(projectile.getLocation().add(random.nextInt(10)-5, random.nextDouble(), random.nextInt(10)-5), Firework.class);
			FireworkMeta fm = firework.getFireworkMeta();
			fm.addEffect(FireworkEffect.builder().withColor(Color.fromRGB(random.nextInt(255), random.nextInt(255), random.nextInt(255))).with(FireworkEffect.Type.values()[random.nextInt(FireworkEffect.Type.values().length)]).build());
			firework.setVelocity(projectile.getVelocity().divide(new Vector(6, 3, 6)));
			fm.setPower(0);
			firework.setFireworkMeta(fm);
		}
	}

	private int getNumFireworks(){
		switch(info.getCurrentLVL()){
			case 0:
				return 1;
			case 1:
				return 1;
			case 2:
				return 2;
			case 3: return 3;
            case 4: return 5;
			default: return info.getCurrentLVL() * 3;
		}
	}

	@Override
	public void onProjectileHit(ProjectileHitEvent event) {
	}
}

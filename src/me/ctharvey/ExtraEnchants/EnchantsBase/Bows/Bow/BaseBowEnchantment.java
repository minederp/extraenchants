/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase.Bows.Bow;

import me.ctharvey.ExtraEnchants.EnchantsBase.BaseEnchantment;
import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.EnchantBowType;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantInfo;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantItemType;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author charles
 */
public abstract class BaseBowEnchantment extends BaseEnchantment {

    public BaseBowEnchantment(EnchantInfo info, Player player, ItemStack is) {
        super(info, EnchantItemType.BOW, EnchantBowType.BOW, player, is);
    }

    @Override
    public abstract void onBowFire(EntityShootBowEvent e);

    @Override
    public abstract void onProjectileHit(ProjectileHitEvent e);
}

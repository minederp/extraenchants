/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase.Bows.Bow;

import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.EnchantBowType;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantInfo;
import org.bukkit.ChatColor;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author charles
 */
public class EnderBow extends BaseBowEnchantment {
	
	private static final EnchantBowType type = EnchantBowType.BOW; 

	public EnderBow(Player player, ItemStack is, int currentLVL) {
        super(new EnchantInfo("EnderBow", "Use your bow to go further!", ChatColor.GOLD, currentLVL, 3), player, is);
		
	}

	@Override
	public void onBowFire(EntityShootBowEvent e) {
		if(e.getEntity().equals(player)){
			fireEnderGun(e);
		}
	}

	private void fireEnderGun(EntityShootBowEvent e) {
		Entity projectile = e.getProjectile();
		e.setCancelled(true);
		EnderPearl pearl = player.launchProjectile(EnderPearl.class);
		pearl.setVelocity(projectile.getVelocity());
		pearl.setShooter(player);
	}

	@Override
	public void onProjectileHit(ProjectileHitEvent event) {
	}
	
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase.Bows;

import java.util.Arrays;
import java.util.List;
import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.BowEnchantIndex;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantItemType;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantItemType;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantSuperType;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantSuperType;
import me.ctharvey.ExtraEnchants.EnchantsBase.Index;
import org.bukkit.Material;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 *
 * @author charles
 */
public enum EnchantBowType implements EnchantSuperType  {
	
	BOW(Material.BOW, Arrays.asList(ProjectileHitEvent.class.getName(), EntityShootBowEvent.class.getName())),
	ARROW(Material.ARROW, Arrays.asList(PlayerInteractEvent.class.getName()));

	public String name;
	public int id;
	public EnchantItemType enchantItemType = EnchantItemType.BOW;
	private List<Material> applicableTypes;
    private List<String> eventNamesUsed;
    
	EnchantBowType(List<Material> applicableTypes, List<String> eventNamesUsed){
		this.name = this.toString().toLowerCase();
		this.id = ordinal();
        this.applicableTypes = applicableTypes;
        this.eventNamesUsed = eventNamesUsed;
	}
    
    EnchantBowType(Material material, List<String> eventNamesUsed){
		this.name = this.toString().toLowerCase();
		this.id = ordinal();
        this.applicableTypes = Arrays.asList(material);
        this.eventNamesUsed = eventNamesUsed;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public EnchantItemType getEnchantItemType() {
		return this.enchantItemType;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public BowEnchantIndex[] getSubtype() {
		switch(this){
			case BOW:
				return BowEnchantIndex.values();
			case ARROW:
				return null;
			default:
				return null;
		}
	}

    /**
     * @return the applicableTypes
     */

    @Override
    public List<Material> getApplicableMaterials() {
        return applicableTypes;
    }

    /**
     * @return the eventNamesUsed
     */
    public List<String> getEventNamesUsed() {
        return eventNamesUsed;
    }
    
    
}

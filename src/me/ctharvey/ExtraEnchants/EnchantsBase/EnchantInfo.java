/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase;

import org.bukkit.ChatColor;

/**
 *
 * @author charles
 */
public class EnchantInfo {
    
    private String name, description;
    private ChatColor color;
    private int currentLVL, maxLVL;

    public EnchantInfo(String name, String description, ChatColor color, int currentLVL, int maxLVL) {
        this.name = name;
        this.description = description;
        this.color = color;
        this.currentLVL = currentLVL;
        this.maxLVL = maxLVL;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the color
     */
    public ChatColor getColor() {
        return color;
    }

    /**
     * @return the currentLVL
     */
    public int getCurrentLVL() {
        return currentLVL;
    }

    /**
     * @param currentLVL the currentLVL to set
     */
    public void setCurrentLVL(int currentLVL) {
        this.currentLVL = currentLVL;
    }

    /**
     * @return the maxLVL
     */
    public int getMaxLVL() {
        return maxLVL;
    }
    
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase;

import java.util.List;
import org.bukkit.Material;

/**
 *
 * @author charles
 */
public enum EnchantWeaponType implements EnchantSuperType {
	
	SWORD,
	DAMAGE_POTIONS;

	public String name;
	
	public int id;
	
	public EnchantItemType enchantItemType = EnchantItemType.WEAPON;
	
	EnchantWeaponType(){
		this.name = this.toString().toLowerCase();
		this.id = ordinal();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public EnchantItemType getEnchantItemType() {
		return this.enchantItemType;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public Index[] getSubtype() {
		return null;
	}

    @Override
    public List<Material> getApplicableMaterials() {
        return null;
    }

    @Override
    public List<String> getEventNamesUsed() {
        return null;
    }
	
}

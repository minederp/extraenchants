/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase;

import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.EnchantBowType;
import Utilities.Helper;
import me.ctharvey.ExtraEnchants.ExtraEnchants;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author charles
 */
public enum EnchantItemType {
	
	TOOL,
	WEAPON,
	ARMOR,
	BOW;
	
	public String name;
	
	public int id;
	
	EnchantItemType(){
		this.name = this.toString().toLowerCase();
		this.id = ordinal();
	}

	public String getName() {
		return this.name;
	}

	public int getId() {
		return id;
	}
	
	public BaseEnchantment getEnchantment(String name, Player player, int currentLVL, ItemStack is){
		EnchantSuperType[] subTypes = getSubTypes();
		for(int x = 0; x < subTypes.length; x++){
			Index[] subtype = subTypes[x].getSubtype();
			if(subtype!= null){
				for(int y = 0; y < subtype.length; y++){
					if(subtype[y].getName().equalsIgnoreCase(name)) {
						return subtype[y].getEnchantment(player, currentLVL, is);
					}
				}
			}
		}
		return null;
	}
	
	public static EnchantSuperType[] getSubTypes(EnchantItemType type){
		return type.getSubTypes();
	}
	
	public EnchantSuperType[] getSubTypes(){
		switch(this){
			case ARMOR:
				return EnchantArmorType.values();
			case TOOL:
				return EnchantToolType.values();
			case WEAPON:
				return EnchantWeaponType.values();
			case BOW:
				return EnchantBowType.values();
			default:
				return null;
		}
	}
	
}

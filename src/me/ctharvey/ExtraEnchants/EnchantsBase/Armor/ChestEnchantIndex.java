/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase.Armor;

import me.ctharvey.ExtraEnchants.EnchantsBase.Armor.Chest.ArrowReflect;
import me.ctharvey.ExtraEnchants.EnchantsBase.BaseEnchantment;
import me.ctharvey.ExtraEnchants.EnchantsBase.Bows.EnchantBowType;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantArmorType;
import me.ctharvey.ExtraEnchants.EnchantsBase.Index;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author charles
 */
public enum ChestEnchantIndex implements Index {
    
    ARROWREFLECT;
    
    ChestEnchantIndex(){
		this.name = this.toString();
		this.id = ordinal();
	}
	
	private String name;
	private int id;
	private EnchantArmorType type = EnchantArmorType.CHEST;

    @Override
    public BaseEnchantment getEnchantment(Player player, int currentLVL, ItemStack is) {
        switch(this){
            case ARROWREFLECT:
                return new ArrowReflect(player, currentLVL, is);
        }
        return null;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
	public Index[] getSubtypes() {
		return values();
	}
    
}

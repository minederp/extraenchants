/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase.Armor.Chest;

import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantInfo;
import me.ctharvey.ExtraEnchants.ExtraEnchants;
import org.bukkit.ChatColor;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author charles
 */
public class ArrowReflect extends BaseChestEnchantment {

    public ArrowReflect(Player player, int currentLVL, ItemStack is) {
        super(new EnchantInfo("ArrowReflect", "Sometimes reflects arrows off armor", ChatColor.AQUA, currentLVL, 4), player, is);
    }

    @Override
    public void onDamageDoneToPlayer(EntityDamageByEntityEvent e) {
        if(e.getDamager() instanceof Projectile && e.getEntity() instanceof Player){
            if(ExtraEnchants.rand.nextInt(100) <= translateLVLtoValue()){
                Projectile projectile = (Projectile) e.getDamager();
                Arrow launchProjectile = ((Player)e.getEntity()).launchProjectile(Arrow.class);
                launchProjectile.setVelocity(projectile.getVelocity().multiply(-1));
                e.setCancelled(true);
            }
        }
    }

    @Override
    public void onProjectileHit(ProjectileHitEvent event) {
    }

    @Override
    public void onItemStackInteract(PlayerInteractEvent e) {
    }

    public double translateLVLtoValue(){
        switch(info.getCurrentLVL()){
            case 0: return 0;
            case 1: return 10;
            case 2: return 20;
            case 3: return 25;
            case 4: return 30;
            default: return info.getCurrentLVL() * 10;
        }
    }
    
}

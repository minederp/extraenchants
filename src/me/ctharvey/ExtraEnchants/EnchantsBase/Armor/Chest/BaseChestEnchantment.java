/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase.Armor.Chest;

import me.ctharvey.ExtraEnchants.EnchantsBase.BaseEnchantment;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantArmorType;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantInfo;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantItemType;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author charles
 */
public abstract class BaseChestEnchantment extends BaseEnchantment {

    public BaseChestEnchantment(EnchantInfo info, Player player, ItemStack is) {
        super(info, EnchantItemType.ARMOR, EnchantArmorType.CHEST, player, is);
    }

    @Override
    public abstract void onDamageDoneToPlayer(EntityDamageByEntityEvent e);

    @Override
    protected void givingDamage(EntityDamageByEntityEvent e) {}

    @Override
    public abstract void onProjectileHit(ProjectileHitEvent event);
}

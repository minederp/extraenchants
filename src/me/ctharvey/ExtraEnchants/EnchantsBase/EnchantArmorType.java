/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.EnchantsBase;

import java.util.Arrays;
import java.util.List;
import me.ctharvey.ExtraEnchants.EnchantController;
import me.ctharvey.ExtraEnchants.EnchantsBase.Armor.ChestEnchantIndex;
import me.ctharvey.ExtraEnchants.ExtraEnchants;
import org.bukkit.Material;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 *
 * @author charles
 */
public enum EnchantArmorType implements EnchantSuperType {
	
	HELMET(EnchantController.helmets, Arrays.asList(EntityDamageByEntityEvent.class.getName())),
	CHEST(EnchantController.chestplates, Arrays.asList(EntityDamageByEntityEvent.class.getName())),
	LEGGINGS(EnchantController.leggings, Arrays.asList(EntityDamageByEntityEvent.class.getName())),
	BOOTS(EnchantController.boots, Arrays.asList(EntityDamageByEntityEvent.class.getName()));

	public String name;
    private List<String> eventNames;
	public int id;
	public EnchantItemType enchantItemType = EnchantItemType.TOOL;
	private List<Material> applicableMaterials;
    
	EnchantArmorType(List<Material> material, List<String> eventNames){
		this.name = this.toString().toLowerCase();
		this.id = ordinal();
        this.applicableMaterials = material;
        this.eventNames = eventNames;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public EnchantItemType getEnchantItemType() {
		return this.enchantItemType;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public Index[] getSubtype() {
		return ChestEnchantIndex.values();
	}
    
    @Override
    public List<Material> getApplicableMaterials(){
        return applicableMaterials;
    }

    @Override
    public List<String> getEventNamesUsed() {
        return eventNames;
    }
}

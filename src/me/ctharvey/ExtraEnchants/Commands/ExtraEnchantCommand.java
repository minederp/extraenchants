/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants.Commands;

import Utilities.Helper;
import java.util.List;
import me.ctharvey.ExtraEnchants.EnchantController;
import me.ctharvey.ExtraEnchants.EnchantsBase.BaseEnchantment;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantItemType;
import me.ctharvey.ExtraEnchants.EnchantsBase.EnchantSuperType;
import me.ctharvey.ExtraEnchants.EnchantsBase.Index;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author charles
 */
public class ExtraEnchantCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
		if(cs instanceof Player){
			Player player = (Player) cs;
			ItemStack itemInHand = player.getItemInHand();
            if(itemInHand == null){
                Helper.send(cs, "Please equip an item before attempting to enchant.");
            }
			String enchantName = null;
			int currentLvl = 0;
			switch(strings.length){
				case 0:
                    StringBuilder sb = new StringBuilder("Choose from following extra-enchants: ");
                    for(EnchantItemType type: EnchantItemType.values()){
                        for(EnchantSuperType sType: type.getSubTypes()){
                        List<Material> applicableMaterials = sType.getApplicableMaterials();
                            if(applicableMaterials!= null && applicableMaterials.contains(itemInHand.getType())){
                                Index[] subtype = sType.getSubtype();
                                if(subtype != null){
                                    for(Index index: subtype){
                                        sb.append(index.getName()).append(" ");
                                    }
                                }
                            }
                        }
                    }
                    Helper.send(cs, sb.toString());
					Helper.send(cs, "Please use command as follows: /enchant enchant_name lvl");
					return true;
				case 1:
					enchantName = strings[0];
					currentLvl = 1;
					break;
				case 2:
					enchantName = strings[0];
					currentLvl = Integer.valueOf(strings[1]);
					break;
			}
			BaseEnchantment enchantment = BaseEnchantment.createEnchantment(enchantName, player, currentLvl, itemInHand);
			if(enchantment != null) {
				enchantment.activate();
            }else {
                StringBuilder sb = new StringBuilder("Choose from following extra-enchants: ");
                for(EnchantItemType type: EnchantItemType.values()){
                    for(EnchantSuperType sType: type.getSubTypes()){
                        if(sType.getApplicableMaterials().contains(itemInHand.getType())){
                            Index[] subtype = sType.getSubtype();
                            for(Index index: subtype){
                                sb.append(index.getName());
                            }
                        }
                    }
                }
				Helper.send(cs, "Unable to locate "+enchantName);
                Helper.send(cs, sb.toString());
			}

			}
		return true;
	}
	
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.ExtraEnchants;

import Storage.ConfigOpt;
import Utilities.Helper;
import java.util.Random;
import me.ctharvey.ExtraEnchants.Commands.ExtraEnchantCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author charles
 */
public class ExtraEnchants extends JavaPlugin {
	
	private static ConfigOpt configOpt;
	private static FileConfiguration config;
	private static EnchantController controller;
	private static ExtraEnchants pl;
    public static Random rand = new Random();

	public static Plugin getPlugin() {
		return pl;
	}
	
	@Override
	public void onEnable(){
		pl = this;
		configOpt = new ConfigOpt(ChatColor.DARK_BLUE, this);
		configOpt.enablePlugin();
		config = getConfig();
		controller = new EnchantController();
		saveDefaultConfig();
		Bukkit.getPluginManager().registerEvents(controller, this);
		getCommand("eenchant").setExecutor(new ExtraEnchantCommand());
	};
	
	@Override
	public void onDisable(){
		configOpt.disablePlugin();
	};
	
	public static ConfigOpt getConfigOpt(){
		return configOpt;
	}

	public static EnchantController getController() {
		return controller;
	}
	
	public static void log(Object msg){
		Helper.newLog(configOpt.getPrefix(), msg);
	}
	
}
